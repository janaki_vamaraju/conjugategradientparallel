#include<stdio.h>
#include<stdlib.h>
#define N 5000  /* size of our input matrices*/
#include<time.h>
#include<math.h>

#include"serial.h"

int main()
{
  int i;/* variables for loops*/
  int j;

/*matrix*/
/*Use double , you have floating numbers not int*/

double A[N][N];
double B[N];
double X[N];
double error = 2.0;
int count = 0;
double *p;
double *t1,*r ,*d0,temp,*t2,*lol1,lol2,alpha, *scalar1,*scalar2,beta1,temp2,*scalar3,*err,*finalX; 
clock_t sec1,sec2;
double time;
t1 = malloc(N*sizeof(double));
r = malloc(N*sizeof(double));
d0 = malloc(N*sizeof(double));
t2 = malloc(N*sizeof(double));
lol1 = malloc(N*sizeof(double));
scalar1 = malloc(N*sizeof(double));
scalar2 = malloc(N*sizeof(double));
scalar3 = malloc(N*sizeof(double));
err = malloc(N*sizeof(double));
finalX = malloc(N*sizeof(double));


  FILE *file;
  file=fopen("matrixA.txt", "r");  /*input for Matrix A , vector b and initial guess x0*/
for(i = 0; i < N; i++)
  {
  for(j = 0; j < N; j++) 
      {
   if (!fscanf(file, "%lf", &A[i][j])) 
          break;
  /*printf("%lf\n",A[i][j]);*/ 
  }
  }
   fclose(file);
                                          

  file=fopen("matrixB.txt", "r");

 for(i = 0; i < N; i++)
  {
  if (!fscanf(file, "%lf", &B[i]))  
          break;
/*printf("%lf\n",B[i]);*/   
 }
 fclose(file);
file=fopen("guessX.txt", "r");

 for(i = 0; i < N; i++)
  {
  if (!fscanf(file, "%lf", &X[i]))
          break;
/*printf("%lf\n",X[i]);*/ 
 }
 fclose(file);

matrixvectorprod( A,X,t1);
vectordiff( B ,t1,r);
for(i = 0;i<N;i++)
{
d0[i] = r[i];
}
sec1 = clock();
// conjugate gradient algorithm implementation (final report.pdf)
while(error > 0.1e-50)
{
temp = innerprod(r,r);
for(i = 0;i<N;i++)
{
t2[i] = X[i];
}
matrixvectorprod(A,d0,lol1);
lol2 = innerprod(d0,lol1);
alpha =  temp/lol2;
scalarvec(alpha,d0,scalar1);
vectoradd(X,scalar1,X);
scalarvec(alpha,lol1,scalar2);
vectordiff(r,scalar2,r);

temp2 = innerprod(r,r);
beta1 = temp2/temp;
scalarvec(beta1,d0,scalar3);
vectoradd(r,scalar3,d0);
vectordiff(X,t2,err);
error = norm2(err);
count = count +1;
}
sec2 = clock();
time = (double)(sec2-sec1)/CLOCKS_PER_SEC ;
for(i = 0;i<N;i++)
{
finalX[i]=X[i];
}

for(i = 0;i<N;i++)
{
printf(" %20.18f\n", finalX[i]);
}
printf("time taken for convergence is %f seconds \n",time);

}





                                          
