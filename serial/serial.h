 #include<math.h>

#define N 5000
/* function for matrix vector multiplication*/
void matrixvectorprod(double AA[N][N] , double XX[N],double result[])
{
int i,j;
double temp[N];

double sum;
for(i = 0; i < N; i++)
  {
sum = 0;
  for(j = 0; j < N; j++) 
      {
 temp[i] = AA[i][j]*XX[j];
sum = sum + temp[i];
}
result[i] = sum;
}
}
/* function for difference of two vectors*/
void vectordiff(double BB[N] ,double AX[N],double diffv[])
{
int i;

for(i = 0; i < N; i++)
{
diffv[i] = BB[i] - AX[i];
}
}
/*function for addition of two vectors*/
void vectoradd(double XX[N] ,double AA[N],double sumv[])
{
int i;

for(i = 0; i < N; i++)
{
sumv[i] = XX[i] + AA[i];
}
}
/*function for scalar vector product*/
void scalarvec(double alpha , double XX[N],double sprod[])
{
int i;

for(i = 0; i < N;i++)
{
sprod[i] = alpha*XX[i];
}
}
/*function for inner products*/
double innerprod(double AA[N], double XX[N])
{
int i;
double beta,sum,temp;
sum = 0;
for (i = 0; i < N;i++)
{
temp = AA[i]*XX[i];
sum = sum + temp;
}
beta = sum;
return beta;
}
/*function for calculating 2 norm of a vector*/
double norm2(double XX[N])
{
int i;
double sum = 0;
for (i=0;i<N;i++)
{
sum = sum +(XX[i]*XX[i]);
}
sum = sqrt(sum);
return sum;
}






