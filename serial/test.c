#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define N 2
#include"serial.h"


int main()
{
double A[2][2] = {{3.0,2.0},{2.0,6.0}};
double B[2] = {2.0,-8.0};
double X[2] = {0.0,8.0};

int error = 2;
int count = 0;
int i;
double *p;
double t1[N],r[N] ,d0[N],temp,t2[N],lol1[N],lol2,alpha, scalar1[N],scalar2[N],beta1,temp2,scalar3[N],err[N]; 
p = matrixvectorprod( A,X);
for(i = 0;i<N;i++)
{
t1[i] = *(p+i);
}
p = vectordiff( B ,t1);
for(i = 0;i<N;i++)
{
r[i] = *(p+i);
}
for(i = 0;i<N;i++)
{
d0[i] = r[i];
}
while(error > 0.01)
{
temp = innerprod(r,r);
for(i = 0;i<N;i++)
{
t2[i] = X[i];
}

p = matrixvectorprod(A,d0);
for(i = 0;i<N;i++)
{
lol1[i] = *(p+i);
}
lol2 = innerprod(d0,lol1);
alpha =  temp/lol2;
p = scalarvec(alpha,d0);
for(i = 0;i<N;i++)
{
scalar1[i] = *(p+i);
}

p = vectoradd(X,scalar1);
for(i = 0;i<N;i++)
{
X[i] = *(p+i);
}

p = scalarvec(alpha,lol1);
for(i = 0;i<N;i++)
{
scalar2[i] = *(p+i);
}


p = vectordiff(r,scalar2);
for(i = 0;i<N;i++)
{
r[i] = *(p+i);
}


temp2 = innerprod(r,r);
beta1 = temp2/temp;
p = scalarvec(beta1,d0);
for(i = 0;i<N;i++)
{
scalar3[i] = *(p+i);
}

p = vectoradd(r,scalar3);
for(i = 0;i<N;i++)
{
d0[i] = *(p+i);
}
p = vectordiff(X,t2);
for(i = 0;i<N;i++)
{
err[i] = *(p+i);
}
error = norm2(err);
count = count +1;
}
}

