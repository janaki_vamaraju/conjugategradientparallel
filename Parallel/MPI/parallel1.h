#include<math.h>
#include<mpi.h>

#define N 4884
/* function for matrix vector multiplication*/
void matrixvectorprod(double AA[][N] , double XX[],int rchunk,double result[])
{
int i,j;
double temp[N];

double sum;

for(i = 0; i < rchunk; i++)
  {
sum = 0;
  for(j = 0; j < N; j++) 
      {
 temp[i] = AA[i][j]*XX[j];
sum = sum + temp[i];
}
result[i] = sum;
}

}
/* function for difference of two vectors*/
void vectordiff(double BB[] ,double AX[],int rchunk,double rloc1[])
{
int i;
for(i = 0; i < rchunk; i++)
{
/*printf("BB[i] = %lf ,AX[i] = %lf,i= %d\n",BB[i],AX[i],i);*/ 
rloc1[i] = BB[i] - AX[i];

}

}
/*function for addition of two vectors*/
void vectoradd(double XX[] ,double AA[],int rchunk,double sumv[])
{
int i;
for(i = 0; i < rchunk; i++)
{
sumv[i] = XX[i] + AA[i];
}

}
/*function for scalar vector product*/
void scalarvec(double alpha , double XX[],int rchunk,double sprod[])
{
int i;

for(i = 0; i < rchunk;i++)
{
sprod[i] = alpha*XX[i];
}

}
/*function for inner products*/
double innerprod(double AA[], double XX[],int rchunk)
{
int i;
double sum,temp;
double beta;
sum = 0;
for (i = 0; i < rchunk;i++)
{
temp = AA[i]*XX[i];
sum = sum + temp;
}
beta = sum;
return beta;
}
/*function for calculating 2 norm of a vector*/
double norm2(double XX[],int rchunk)
{
int i;
double sum = 0;
for (i=0;i<rchunk;i++)
{
sum = sum +(XX[i]*XX[i]);
}
return sum;

}






