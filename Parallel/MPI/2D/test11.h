

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"
#define N 1120
matrixvectorprod(int NoofRows_Bloc, int NoofCols_Bloc, double Bloc_Matrix[], double Bloc_Vector[],double MyResult[]) 
{
int irow,icol;
  /* Multiplication done by all procs */

  
  int index = 0;
  for(irow=0; irow < NoofRows_Bloc; irow++){
      MyResult[irow]=0;
      for(icol=0;icol< NoofCols_Bloc; icol++){
          MyResult[irow] += Bloc_Matrix[index++] * Bloc_Vector[icol];
      }
  }

   }



