#include<stdio.h>
#include<stdlib.h>
#define N 4884
#include<math.h>
#include"mpi.h"
#include "test1.h"

#include"parallel1.h"

int main (int argc , char *argv[]) 
{
MPI_Status status;     
  MPI_Comm row_comm, col_comm;
  MPI_Group MPI_GROUP_WORLD;
int numtasks,rank,count;
int i,j,chunk,rchunk,root_p, Root = 0;;
double temp1,temp,error,temp3,lol2,alpha,temp2,beta1,errorone,time1,time2;
double A[N][N];
double B[N],Aarray[N*N];
double *X, *Blocal, *Xlocal, *Alocal, *temppp, *rloc, *r, *d0, *dlocal, *tt2, *scalar1, *AXloc; 
double *finalX;
int irow, icol, iproc, jproc, index, Proc_Id;
int  NoofRows_Bloc, NoofCols_Bloc;
int Bloc_MatrixSize, Bloc_VectorSize ;
int VectorSize=N;
int Local_Index, Global_Row_Index, Global_Col_Index;

double **Matrix, *Matrix_Array, *Bloc_Matrix, *Vector, *Bloc_Vector, *FinalVector,*d0_Vector, *FinalResult2, *FinalVector2;
double *FinalResult, *MyResult;
int *ranks, colsize, colrank, rowsize, rowrank, myrow;
int MatrixFileStatus = 1, VectorFileStatus = 1;

  FILE *fp;
X=malloc(N*sizeof(double));
MPI_Init(&argc,&argv);
MPI_Comm_group(MPI_COMM_WORLD,&MPI_GROUP_WORLD);
MPI_Comm_size(MPI_COMM_WORLD ,&numtasks);
MPI_Comm_rank(MPI_COMM_WORLD,&rank);
int sendcountt[numtasks],displacement[numtasks],displacement2[numtasks],sendcount[numtasks];
chunk = N/numtasks;
if (rank == numtasks-1)
rchunk = N - (chunk*(numtasks-1));
else 
rchunk = chunk;
Blocal=malloc(rchunk*sizeof(double));
Xlocal=malloc(rchunk*sizeof(double));
AXloc = malloc(rchunk*sizeof(double));
Alocal=malloc(rchunk*N*sizeof(double));
temppp=malloc(rchunk*sizeof(double));
rloc=malloc(rchunk*sizeof(double));
r = malloc(N*sizeof(double));
d0 = malloc(N*sizeof(double));
dlocal = malloc(rchunk*sizeof(double));
tt2=malloc(rchunk*sizeof(double));
scalar1=malloc(rchunk*sizeof(double));
finalX = malloc(N*sizeof(double));
error = 2.0;
count = 0;
double AAloc[rchunk][N];
  /* .......Variables Initialisation ......*/
  root_p = sqrt((double)numtasks);
  if(numtasks != (root_p * root_p)) {
     if( rank == 0) 
        printf("Error : Number of processors should be perfect square \n");
     MPI_Finalize();
     exit(-1);
  }

  if(rank == 0) 
  {
    FILE *file;
  file=fopen("matrixA.txt", "r");
for(i = 0; i < N; i++)
  {
  for(j = 0; j < N; j++) 
      {
   if (!fscanf(file, "%lf", &A[i][j])) 
          break;
  /*printf("%lf\n",A[i][j]);*/ 
  }
  }
   fclose(file);


file=fopen("matrixB.txt", "r");

 for(i = 0; i < N; i++)
  {
  if (!fscanf(file, "%lf", &B[i]))  
          break;
/*printf("%lf\n",B[i]);*/   
 }
 fclose(file);
file=fopen("guessX.txt", "r");

 for(i = 0; i < N; i++)
  {
  if (!fscanf(file, "%lf", &X[i]))
          break;
}
 fclose(file);

   }  /* end  of if myrank = 0 */


MPI_Barrier(MPI_COMM_WORLD);
MPI_Bcast (&MatrixFileStatus, 1, MPI_INT, Root, MPI_COMM_WORLD);
MPI_Bcast (&VectorFileStatus, 1, MPI_INT, Root, MPI_COMM_WORLD);
NoofRows_Bloc = N/root_p;
NoofCols_Bloc = N/root_p;
  if(rank == 0){

      Matrix_Array = (double *)malloc(N*N*sizeof(double));
      /* Convert Matrix into 1-D array according to Checkerboard ......*/
      Local_Index = 0;
      for(iproc = 0; iproc < root_p; iproc++){
         for(jproc = 0; jproc < root_p; jproc++){
   	     Proc_Id = iproc * root_p + jproc;
	     for(irow = 0; irow < NoofRows_Bloc; irow++){
		 Global_Row_Index = iproc * NoofRows_Bloc + irow;
	         for (icol = 0; icol < NoofCols_Bloc; icol++){
		      Global_Col_Index = jproc * NoofCols_Bloc + icol;
	              Matrix_Array[Local_Index++] = 
			       A[Global_Row_Index][Global_Col_Index];
	         }
	     }
	 }
       }
   }


 /* Memory allocating for Bloc Matrix */
  Bloc_VectorSize = VectorSize / root_p;
  Bloc_MatrixSize = NoofRows_Bloc * NoofCols_Bloc;
  Bloc_Matrix = (double *) malloc (Bloc_MatrixSize * sizeof(double));
  MPI_Scatter(Matrix_Array, Bloc_MatrixSize, MPI_DOUBLE, Bloc_Matrix, 
	      Bloc_MatrixSize, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  MPI_Barrier(MPI_COMM_WORLD); 
/* Creating groups of procesors row wise */
  myrow=rank/root_p;
  MPI_Comm_split(MPI_COMM_WORLD,myrow,rank,&row_comm);
  MPI_Comm_size(row_comm,&rowsize);
  MPI_Comm_rank(row_comm,&rowrank);

/* Creating groups of procesors column wise */
  myrow=rank%root_p;
  MPI_Comm_split(MPI_COMM_WORLD,myrow,rank,&col_comm);
  MPI_Comm_size(col_comm,&colsize);
  MPI_Comm_rank(col_comm,&colrank);
 /* Scatter part of vector to all row master processors */
  Bloc_Vector = (double*) malloc(Bloc_VectorSize * sizeof(double));
  if(rank/root_p == 0){
     MPI_Scatter(X, Bloc_VectorSize, MPI_DOUBLE, Bloc_Vector, 
		 Bloc_VectorSize, MPI_DOUBLE, 0,row_comm);
  }


/* Row master broadcasts its vector part to processors in its column */
  MPI_Bcast(Bloc_Vector, Bloc_VectorSize, MPI_DOUBLE, 0, col_comm);
MyResult   = (double*) malloc(NoofRows_Bloc * sizeof(double));
matrixvectorprod(NoofRows_Bloc,NoofCols_Bloc, Bloc_Matrix,Bloc_Vector,MyResult) ;

/* collect partial product from all procs on to master processor 
		and add it to get final answer */
   if(rank == Root) 
      FinalResult = (double *)malloc(NoofRows_Bloc*numtasks*sizeof(double));

   MPI_Gather (MyResult, NoofRows_Bloc, MPI_DOUBLE, FinalResult, 
	       NoofRows_Bloc, MPI_DOUBLE, 0, MPI_COMM_WORLD); 

 if(rank == 0){
      FinalVector = (double *) malloc(N * sizeof(double));
      index = 0;
      for(iproc=0; iproc < root_p; iproc++){
	  for(irow=0; irow < NoofRows_Bloc; irow++){
	      FinalVector[index]  = 0;
	      for(jproc=0; jproc < root_p; jproc++){
		  FinalVector[index] += 
		    FinalResult[iproc*root_p*NoofRows_Bloc + 
				jproc*NoofRows_Bloc +irow];

	      }
              /*intf(" FinalVector[%d] = %7.3f\n",index,FinalVector[index]);*/
	      index++;
	  }
      }
   }
 

for(i=0; i < numtasks;i++)
{
if ( i == numtasks-1)
{
sendcount[i] = N - (chunk*(numtasks-1));
sendcountt[i] = sendcount[i]*N;
}
else
{
sendcount[i] = chunk;
sendcountt[i] = sendcount[i]*N;
}
displacement[i] = chunk*i;
displacement2[i] = i*N*chunk;
}

MPI_Scatterv(&B[0],sendcount,displacement,MPI_DOUBLE,&Blocal[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);
MPI_Scatterv(&X[0],sendcount,displacement,MPI_DOUBLE,&Xlocal[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);
MPI_Scatterv(&FinalVector[0],sendcount,displacement,MPI_DOUBLE,&AXloc[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);
MPI_Bcast(&X[0],N,MPI_DOUBLE,0,MPI_COMM_WORLD);
vectordiff(Blocal,AXloc,rchunk,rloc);
MPI_Allgatherv(&rloc[0],rchunk,MPI_DOUBLE,&r[0],sendcount,displacement,MPI_DOUBLE,MPI_COMM_WORLD);
for(i = 0;i<N;i++)
{
d0[i] = r[i];
}
MPI_Scatterv(&d0[0],sendcount,displacement,MPI_DOUBLE,&dlocal[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);
d0_Vector = (double*) malloc(Bloc_VectorSize * sizeof(double));
if(rank/root_p == 0){
     MPI_Scatter(d0, Bloc_VectorSize, MPI_DOUBLE, d0_Vector, 
		 Bloc_VectorSize, MPI_DOUBLE, 0,row_comm);
  }
MPI_Bcast(d0_Vector, Bloc_VectorSize, MPI_DOUBLE, 0, col_comm);

time1 = MPI_Wtime();
// conjugate gradient implementation (2D block distribution version)
while(error > 0.1e-50)
{
temp1 = innerprod(rloc,rloc,rchunk);
MPI_Allreduce(&temp1,&temp,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
for(i = 0;i<rchunk;i++)
{
tt2[i] = Xlocal[i];
}
matrixvectorprod(NoofRows_Bloc,NoofCols_Bloc, Bloc_Matrix,d0_Vector,MyResult);



if(rank == Root) 
 FinalResult2= (double *)malloc(NoofRows_Bloc*numtasks*sizeof(double));

      MPI_Gather (MyResult, NoofRows_Bloc, MPI_DOUBLE, FinalResult2,
	       NoofRows_Bloc, MPI_DOUBLE, 0, MPI_COMM_WORLD); 
if(rank == 0){
 FinalVector2= (double *) malloc(N * sizeof(double));
      index = 0;
      for(iproc=0; iproc < root_p; iproc++){
	  for(irow=0; irow < NoofRows_Bloc; irow++){
	      FinalVector2[index]  = 0;
	      for(jproc=0; jproc < root_p; jproc++){
		  FinalVector2[index] += 
		    FinalResult2[iproc*root_p*NoofRows_Bloc + 
				jproc*NoofRows_Bloc +irow];

	      }
             /* printf(" FinalVector[%d] = %7.3f\n",index,FinalVector2[index]);*/
	      index++;
	  }
      }
   }

MPI_Scatterv(&FinalVector2[0],sendcount,displacement,MPI_DOUBLE,&temppp[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);
temp3 = innerprod(dlocal,temppp,rchunk);
MPI_Allreduce(&temp3,&lol2,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
alpha =  temp/lol2;

scalarvec(alpha,dlocal,rchunk,scalar1);
vectoradd(Xlocal,scalar1,rchunk,Xlocal);
MPI_Allgatherv(&Xlocal[0],rchunk,MPI_DOUBLE,&X[0],sendcount,displacement,MPI_DOUBLE,MPI_COMM_WORLD);


scalarvec(alpha,temppp,rchunk,scalar1);
vectordiff(rloc,scalar1,rchunk,rloc);
MPI_Allgatherv(&rloc[0],rchunk,MPI_DOUBLE,&r[0],sendcount,displacement,MPI_DOUBLE,MPI_COMM_WORLD);


temp1 = innerprod(rloc,rloc,rchunk);
MPI_Allreduce(&temp1,&temp2,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
beta1 = temp2/temp;

scalarvec(beta1,dlocal,rchunk,scalar1);
vectoradd(rloc,scalar1,rchunk,dlocal);
MPI_Allgatherv(&dlocal[0],rchunk,MPI_DOUBLE,&d0[0],sendcount,displacement,MPI_DOUBLE,MPI_COMM_WORLD);

if(rank/root_p == 0){
     MPI_Scatter(d0, Bloc_VectorSize, MPI_DOUBLE, d0_Vector, 
		 Bloc_VectorSize, MPI_DOUBLE, 0,row_comm);
  }
MPI_Bcast(d0_Vector, Bloc_VectorSize, MPI_DOUBLE, 0, col_comm);
vectordiff(Xlocal,tt2,rchunk,scalar1);
errorone = norm2(scalar1,rchunk);
MPI_Allreduce(&errorone,&error,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
error = sqrt(error);
count = count +1;



}
time2 = MPI_Wtime();
MPI_Gatherv(&Xlocal[0],rchunk,MPI_DOUBLE,&finalX[0],sendcount,displacement,MPI_DOUBLE,0,MPI_COMM_WORLD);

if (rank == 0)
{
for(i = 0;i<N;i++)
{
printf(" %20.18f\n", finalX[i]);
}
}
printf("rank is %d time taken is %20.18f\n",rank,time2-time1);



MPI_Finalize();
}





 
