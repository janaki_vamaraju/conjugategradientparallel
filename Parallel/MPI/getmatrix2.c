#include<stdio.h>
#include<stdlib.h>
#define N 4884
#include<math.h>
#include"mpi.h"

#include"parallel2.h"

int main (int argc , char *argv[]) 
{
int numtasks,rank,count;
int i,j,chunk,rchunk;
double temp1,temp,error,temp3,lol2,alpha,temp2,beta1,errorone,time1,time2;
double A[N][N];
double B[N],Aarray[N*N];
double *X, *Blocal, *Xlocal, *Alocal, *temppp, *temppp2, *temploc, *rloc, *r, *d0, *dlocal, *tt2, *scalar1, *finalX;
X=malloc(N*sizeof(double));
MPI_Init(&argc,&argv);
MPI_Comm_size(MPI_COMM_WORLD ,&numtasks);
MPI_Comm_rank(MPI_COMM_WORLD,&rank);
int sendcountt[numtasks],displacement[numtasks],displacement2[numtasks],sendcount[numtasks];
chunk = N/numtasks;
if (rank == numtasks-1)
rchunk = N - (chunk*(numtasks-1));
else 
rchunk = chunk;
Blocal=malloc(rchunk*sizeof(double));
Xlocal=malloc(rchunk*sizeof(double));
Alocal=malloc(rchunk*N*sizeof(double));
temppp=malloc(N*sizeof(double));
temppp2=malloc(N*sizeof(double));
temploc=malloc(rchunk*sizeof(double));
rloc=malloc(rchunk*sizeof(double));
r = malloc(N*sizeof(double));
d0 = malloc(N*sizeof(double));
dlocal = malloc(rchunk*sizeof(double));
tt2=malloc(rchunk*sizeof(double));
scalar1=malloc(rchunk*sizeof(double));
finalX = malloc(N*sizeof(double));
error = 2.0;
count = 0;
double AAloc[N][rchunk];
 /*input for Matrix A , vector b and initial guess x0*/
if(rank == 0)
{
FILE *file;
  file=fopen("matrixA.txt", "r");
for(i = 0; i < N; i++)
  {
  for(j = 0; j < N; j++) 
      {
   if (!fscanf(file, "%lf", &A[i][j])) 
          break;
  /*printf("%lf\n",A[i][j]);*/ 
  }
  }
   fclose(file);
                                          

  file=fopen("matrixB.txt", "r");

 for(i = 0; i < N; i++)
  {
  if (!fscanf(file, "%lf", &B[i]))  
          break;
/*printf("%lf\n",B[i]);*/   
 }
 fclose(file);
file=fopen("guessX.txt", "r");

 for(i = 0; i < N; i++)
  {
  if (!fscanf(file, "%lf", &X[i]))
          break;
/*printf("%lf\n",X[i]);*/ 
 }
 fclose(file);
 // Distributing arrays across processors
for(i = 0;i<N;i++)
{
for(j = 0;j<N;j++)
{
Aarray[j+(i*N)] = A[j][i];
}
}
}
for(i=0; i < numtasks;i++)
{
if ( i == numtasks-1)
{
sendcount[i] = N - (chunk*(numtasks-1));
sendcountt[i] = sendcount[i]*N;
}
else
{
sendcount[i] = chunk;
sendcountt[i] = sendcount[i]*N;
}
displacement[i] = chunk*i;
displacement2[i] = i*N*chunk;
}
MPI_Scatterv(&B[0],sendcount,displacement,MPI_DOUBLE,&Blocal[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);
MPI_Scatterv(&X[0],sendcount,displacement,MPI_DOUBLE,&Xlocal[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);
MPI_Scatterv(&Aarray[0],sendcountt,displacement2,MPI_DOUBLE,&Alocal[0],rchunk*N,MPI_DOUBLE,0,MPI_COMM_WORLD);
MPI_Bcast(&X[0],N,MPI_DOUBLE,0,MPI_COMM_WORLD);
for(i = 0;i<rchunk;i++)
{
for(j = 0;j<N;j++)
{
AAloc[j][i] = Alocal[j+(i*N)];
}
}
matrixvectorprod(rchunk,AAloc,Xlocal,temppp);
for (i=0;i<N;i++)
{
MPI_Allreduce(&temppp[i],&temppp2[i],1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
}

MPI_Scatterv(&temppp2[0],sendcount,displacement,MPI_DOUBLE,&temploc[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);

vectordiff(Blocal,temploc,rchunk,rloc);
MPI_Allgatherv(&rloc[0],rchunk,MPI_DOUBLE,&r[0],sendcount,displacement,MPI_DOUBLE,MPI_COMM_WORLD);
for(i = 0;i<N;i++)
{
d0[i] = r[i];
}

MPI_Scatterv(&d0[0],sendcount,displacement,MPI_DOUBLE,&dlocal[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);
time1 = MPI_Wtime();

/*if(rank == 0)
{
for(i=0;i<N;i++)
{
printf("r is %f\n",r[i]);
}
}*/

//Conjugate gradient algorithm implementation (1D block column version)
while(error > 0.1e-50)
{
temp1 = innerprod(rloc,rloc,rchunk);
MPI_Allreduce(&temp1,&temp,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
for(i = 0;i<rchunk;i++)
{
tt2[i] = Xlocal[i];
}
matrixvectorprod(rchunk,AAloc,dlocal,temppp);
/*for (i=0;i<N;i++)
*/
MPI_Allreduce(&temppp[0],&temppp2[0],N,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

MPI_Scatterv(&temppp2[0],sendcount,displacement,MPI_DOUBLE,&temploc[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);
temp3 = innerprod(dlocal,temploc,rchunk);
MPI_Allreduce(&temp3,&lol2,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
alpha =  temp/lol2;

scalarvec(alpha,dlocal,rchunk,scalar1);
vectoradd(Xlocal,scalar1,rchunk,Xlocal);
MPI_Allgatherv(&Xlocal[0],rchunk,MPI_DOUBLE,&X[0],sendcount,displacement,MPI_DOUBLE,MPI_COMM_WORLD);

scalarvec(alpha,temploc,rchunk,scalar1);
vectordiff(rloc,scalar1,rchunk,rloc);
MPI_Allgatherv(&rloc[0],rchunk,MPI_DOUBLE,&r[0],sendcount,displacement,MPI_DOUBLE,MPI_COMM_WORLD);

temp1 = innerprod(rloc,rloc,rchunk);
MPI_Allreduce(&temp1,&temp2,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
beta1 = temp2/temp;

scalarvec(beta1,dlocal,rchunk,scalar1);
vectoradd(rloc,scalar1,rchunk,dlocal);
MPI_Allgatherv(&dlocal[0],rchunk,MPI_DOUBLE,&d0[0],sendcount,displacement,MPI_DOUBLE,MPI_COMM_WORLD);

vectordiff(Xlocal,tt2,rchunk,scalar1);
errorone = norm2(scalar1,rchunk);
MPI_Allreduce(&errorone,&error,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
error = sqrt(error);
count = count +1;

}
time2 = MPI_Wtime();
MPI_Gatherv(&Xlocal[0],rchunk,MPI_DOUBLE,&finalX[0],sendcount,displacement,MPI_DOUBLE,0,MPI_COMM_WORLD);

if (rank == 0)
{
for(i = 0;i<N;i++)
{
printf(" %20.18f\n", finalX[i]);
}
}
printf("rank is %d time taken is %20.18f\n",rank,time2-time1);






MPI_Finalize();
}
