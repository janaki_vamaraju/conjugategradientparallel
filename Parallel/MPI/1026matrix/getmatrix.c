#include<stdio.h>
#include<stdlib.h>
#define N 10
#include<math.h>
#include"mpi.h"

#include"parallel1.h"

int main (int argc , char *argv[]) 
{
int numtasks , rank ,count;
int chunk, rchunk,i,j;                                                 
double time1,time2,time3,error,temp1,temp,lol2,alpha,temp2,beta1,errorone,temp3; 
double A[N][N];
double B[N],r[N],d0[N],finalX[N];
double X[N];
double Aarray[N*N];

MPI_Init(&argc,&argv);
MPI_Comm_size(MPI_COMM_WORLD ,&numtasks);
MPI_Comm_rank(MPI_COMM_WORLD,&rank);
int sendcountt[numtasks],displacement[numtasks],displacement2[numtasks],sendcount[numtasks];
chunk = N/numtasks;
if (rank == numtasks-1)
rchunk = N - (chunk*(numtasks-1));
else 
rchunk = chunk;
double Blocal[rchunk],Xlocal[rchunk],temppp[rchunk],rloc[chunk],dlocal[rchunk],tt2[rchunk],scalar1[rchunk],scalar2[rchunk],scalar3[rchunk],err[rchunk];
double Alocal[rchunk*N];
double AAloc[rchunk][N];
error = 2.0;
count = 0;

if(rank == 0)
{

FILE *file;
  file=fopen("matrixA.txt", "r");
for(i = 0; i < N; i++)
  {
  for(j = 0; j < N; j++) 
      {
   if (!fscanf(file, "%lf", &A[i][j])) 
          break;
  /*printf("%lf\n",A[i][j]);*/ 
  }
  }
   fclose(file);
                                          

  file=fopen("matrixB.txt", "r");

 for(i = 0; i < N; i++)
  {
  if (!fscanf(file, "%lf", &B[i]))  
          break;
/*printf("%lf\n",B[i]);*/   
 }
 fclose(file);
file=fopen("guessX.txt", "r");

 for(i = 0; i < N; i++)
  {
  if (!fscanf(file, "%lf", &X[i]))
          break;
/*printf("%lf\n",X[i]);*/ 
 }
 fclose(file);

time1 = MPI_Wtime();
for(i = 0;i<N;i++)
{
for(j = 0;j<N;j++)
{
Aarray[j+(i*N)] = A[i][j];
}
}
}

for(i=0; i < numtasks;i++)
{
if ( i == numtasks-1)
{
sendcount[i] = N - (chunk*(numtasks-1));
sendcountt[i] = sendcount[i]*N;
}
else
{
sendcount[i] = chunk;
sendcountt[i] = sendcount[i]*N;
}
displacement[i] = chunk*i;
displacement2[i] = i*N*chunk;
}
MPI_Scatterv(&B[0],sendcount,displacement,MPI_DOUBLE,&Blocal[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);
MPI_Scatterv(&X[0],sendcount,displacement,MPI_DOUBLE,&Xlocal[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);
MPI_Scatterv(&Aarray[0],sendcountt,displacement2,MPI_DOUBLE,&Alocal[0],rchunk*N,MPI_DOUBLE,0,MPI_COMM_WORLD);
MPI_Bcast(&X[0],N,MPI_DOUBLE,0,MPI_COMM_WORLD);
for(i = 0;i<rchunk;i++)
{
for(j = 0;j<N;j++)
{
AAloc[i][j] = Alocal[j+(i*N)];
}
}
matrixvectorprod(AAloc,X,rchunk,temppp);
vectordiff(Blocal,temppp,rchunk,rloc);
MPI_Allgatherv(&rloc[0],rchunk,MPI_DOUBLE,&r[0],sendcount,displacement,MPI_DOUBLE,MPI_COMM_WORLD);
for(i = 0;i<N;i++)
{
d0[i] = r[i];
}
MPI_Scatterv(&d0[0],sendcount,displacement,MPI_DOUBLE,&dlocal[0],rchunk,MPI_DOUBLE,0,MPI_COMM_WORLD);

while(error > 0.1e-50)
{
temp1 = innerprod(rloc,rloc,rchunk);
MPI_Allreduce(&temp1,&temp,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

for(i = 0;i<rchunk;i++)
{
tt2[i] = Xlocal[i];
}

matrixvectorprod(AAloc,d0,rchunk,temppp);
temp3 = innerprod(dlocal,temppp,rchunk);
MPI_Allreduce(&temp3,&lol2,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
alpha =  temp/lol2;

scalarvec(alpha,dlocal,rchunk,scalar1);
vectoradd(Xlocal,scalar1,rchunk,Xlocal);
MPI_Allgatherv(&Xlocal[0],rchunk,MPI_DOUBLE,&X[0],sendcount,displacement,MPI_DOUBLE,MPI_COMM_WORLD);

scalarvec(alpha,temppp,rchunk,scalar2);
vectordiff(rloc,scalar2,rchunk,rloc);
MPI_Allgatherv(&rloc[0],rchunk,MPI_DOUBLE,&r[0],sendcount,displacement,MPI_DOUBLE,MPI_COMM_WORLD);

temp1 = innerprod(rloc,rloc,rchunk);
MPI_Allreduce(&temp1,&temp2,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
beta1 = temp2/temp;


scalarvec(beta1,dlocal,rchunk,scalar3);
vectoradd(rloc,scalar3,rchunk,dlocal);
MPI_Allgatherv(&dlocal[0],rchunk,MPI_DOUBLE,&d0[0],sendcount,displacement,MPI_DOUBLE,MPI_COMM_WORLD);

vectordiff(Xlocal,tt2,rchunk,err);
errorone = norm2(err,rchunk);
MPI_Allreduce(&errorone,&error,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
error = sqrt(error);
count = count +1;
}
MPI_Gatherv(&Xlocal[0],rchunk,MPI_DOUBLE,&finalX[0],sendcount,displacement,MPI_DOUBLE,0,MPI_COMM_WORLD);

if (rank == 0)
{
time2 = MPI_Wtime();
for(i = 0;i<N;i++)
{
printf(" %20.18f\n", finalX[i]);
}
printf("time taken is %20.18f\n",time2-time1);
}


MPI_Finalize();
}

