#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include"omp.h"

#define N 1806
# define chunk N/
/* function for matrix vector multiplication*/

/*1) outer loop paralelization variant)*/
void matrixvectorprod(double AA[N][N] , double XX[N],double result[])
{
int i,j,i1;
double temp[N];
double sum;
#pragma omp parallel shared(AA,result,XX) private(i,j) 
   {
#pragma omp for  
   for (i=0; i<N; i=i+1){
      result[i]=0.0;
      for (j=0; j<N; j=j+1){
         result[i]=(result[i])+((AA[i][j])*(XX[j]));
      }
   }
   }
   
}


/*inner loop parralelize */ 
/*matrixvectorprod(double AA[N][N] , double XX[N],double result[])
{
int i,j;
double temp;

double sum;
for (i=0; i<N; i=i+1)
{
 temp=0.0;
#pragma omp parallel for schedule(static) shared(AA,result,XX,i) private(j) reduction(+:temp)
 for (j=0; j<N; j=j+1){
         temp =(temp)+((AA[i][j])*(XX[j]));
      }
   result[i] = temp;
   }
   
}

*/



/* function for difference of two vectors (use different schedules and chunk sizes) */
void vectordiff(double BB[N] ,double AX[N],double diffv[])
{
int i;

#pragma omp parallel for shared(BB,diffv,AX) private(i) /*schedule(dynamic,360)*/
for(i = 0; i < N; i++)
{
diffv[i] = BB[i] - AX[i];
}

}
/*function for addition of two vectors (use different schedules and chunk sizes) */
void vectoradd(double XX[N] ,double AA[N], double sumv[])
{
int i;
#pragma omp parallel for shared(XX,sumv,AA) private(i) /*schedule(dynamic,360)*/
for(i = 0; i < N; i++)
{
sumv[i] = XX[i] + AA[i];
}

}
/*function for scalar vector product (use different schedules and chunk sizes)*/
void scalarvec(double alpha , double XX[N],double sprod[])
{
int i;

#pragma omp parallel for shared(alpha,sprod,XX) private(i) /*schedule(dynamic,360)*/
for(i = 0; i < N;i++)
{
sprod[i] = alpha*XX[i];
}

}
/*function for inner products(can be parallelized also by using critical)*/
double innerprod(double AA[N], double XX[N])
{
int i;
double beta,sum,temp;
sum = 0.0;
#pragma omp parallel for reduction(+ : sum) shared(AA,beta,temp,XX) private(i) /*schedule(dynamic,360)*/
for (i = 0; i < N;i++)
{
/*temp = AA[i]*XX[i];*/
sum = sum + (AA[i]*XX[i]);
}
beta = sum;
return beta;
}
/*function for calculating 2 norm of a vector(can be parallelized also by using critical)*/
double norm2(double XX[N])
{
int i;
double sum = 0.0;
#pragma omp parallel for reduction(+ : sum)shared(XX) private(i) /*schedule(dynamic,360)*/
for (i=0;i<N;i++)
{
sum = sum +(XX[i]*XX[i]);
}
sum = sqrt(sum);
return sum;
}






